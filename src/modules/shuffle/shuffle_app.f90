module shuffle_app_module
    use, intrinsic :: iso_fortran_env, only : int64

    use :: util_api, only : &
            string, &
            string_converter, &
            application_config

    use :: data_api, only : &
            stream, &
            dependency_manager

    use :: tensor_api, only : &
            vector, &
            tensor_builder, &
            tensor_initializer, &
            create_tensor_builder, &
            create_tensor_initializer, &
            tensor_datatype_helper, &
            shuffle_factory, &
            shuffle

    implicit none
    private

    public :: shuffle_app

    type :: shuffle_app
        type(vector) :: c, a
        class(shuffle), allocatable :: ashuffle
        type(string) :: shuffle_type
        type(stream) :: my_stream
        integer(int64), dimension(:), allocatable :: src_indices, dst_indices
    contains
        procedure :: initialize => initialize
        procedure :: check_config => check_config
        procedure :: allocate_and_setup_data => allocate_and_setup_data
        procedure :: setup_shuffle => setup_shuffle
        procedure :: run => run
        procedure :: cleanup => cleanup
        procedure :: clear => clear
    end type shuffle_app

    interface shuffle_app
        module procedure constructor_empty
        module procedure constructor
    end interface shuffle_app

contains
    function constructor_empty() result(this)
        type(shuffle_app) :: this

        call this%clear()
    end function constructor_empty

    function constructor(config) result(this)
        type(application_config), intent(in) :: config
        type(shuffle_app) :: this

        this = shuffle_app()
        call this%initialize(config)
    end function constructor

    subroutine initialize(this, config)
        class(shuffle_app), intent(inout) :: this
        type(application_config), intent(in) :: config

        type(string), dimension(1) :: priorities

        priorities = [string("shuffle-")]

        call this%check_config(config, priorities)
        call this%setup_shuffle(config, priorities)
        call this%allocate_and_setup_data( config, priorities)
    end subroutine initialize

    subroutine check_config(this, config, priorities)
        class(shuffle_app), intent(in) :: this
        type(application_config), intent(in) :: config
        type(string), dimension(:), intent(in) :: priorities

        type(string), dimension(:), allocatable :: required
        integer :: idx
        logical :: error

        error = .false.
        required = [ &
                string("vector_size"), &
                string("shuffle_type") &
            ]

        if ( .not. config%config_has_required_options(required, priorities) ) &
                error stop "shuffle_app::check_config:Missing options."
    end subroutine check_config

    subroutine setup_shuffle(this, config, priorities)
        class(shuffle_app), intent(inout) :: this
        type(application_config), intent(in) :: config
        type(string), dimension(:), intent(in) :: priorities

        this%my_stream = dependency_manager%get_new_stream()

        call shuffle_factory%create(this%ashuffle, &
                options=config%full_config, priorities=priorities)
    end subroutine setup_shuffle

    subroutine allocate_and_setup_data(this, config, priorities)
        class(shuffle_app), intent(inout) :: this
        type(application_config), intent(in) :: config
        type(string), dimension(:), intent(in) :: priorities

        type(tensor_builder) :: builder
        class(tensor_initializer), allocatable :: initializer
        integer :: datatype
        integer(int64) :: vector_size
        type(tensor_datatype_helper) :: helper
        type(string_converter) :: converter
        integer(int64) :: idx

        this%shuffle_type = config%full_config%get_value("shuffle_type", priorities)

        datatype = helper%get_datatype(options=config%full_config, priorities=priorities)
        vector_size = converter%toint64(config%full_config%get_value("vector_size", priorities))

        call create_tensor_builder(builder, options=config%full_config, priorities=priorities)
        call create_tensor_initializer(initializer, "random")

        call builder%create(this%c, datatype, [vector_size], .true.)

        call builder%create(this%a, datatype, [vector_size], .false.)
        call initializer%initialize(this%a)

        this%src_indices = [(idx, idx=vector_size, int(1, int64), int(-1, int64))]
        this%dst_indices = this%src_indices
    end subroutine allocate_and_setup_data

    subroutine run(this)
        class(shuffle_app), intent(inout) :: this

        select case( this%shuffle_type%char_array )
        case ("copy")
            call this%ashuffle%execute(this%c, this%a, this%my_stream)
        case ("random")
            call this%ashuffle%execute(this%c, this%a, this%dst_indices, this%src_indices, this%my_stream)
        case default
            error stop 'shuffle_app::run:Shuffle type not recognized.'
        end select
        call this%ashuffle%synchronize(this%my_stream)
    end subroutine run

    subroutine cleanup(this)
        class(shuffle_app), intent(inout) :: this

        integer :: idx

        call dependency_manager%destroy_stream(this%my_stream)

        if ( allocated( this%ashuffle ) ) then
            call this%ashuffle%cleanup()
            deallocate(this%ashuffle)
        end if

        call this%c%cleanup()
        call this%a%cleanup()

        if ( allocated(this%src_indices) ) &
                deallocate(this%src_indices)
        if ( allocated(this%dst_indices) ) &
                deallocate(this%dst_indices)

        call this%clear()
    end subroutine cleanup

    subroutine clear(this)
        class(shuffle_app), intent(inout) :: this

        this%my_stream = stream()
    end subroutine clear
end module shuffle_app_module
