program gemm_example
    use :: algorithms_initializer, only : &
            algorithms_initialize, &
            algorithms_finalize
    
    use :: gemm_example_module, only : run_gemm_example

    implicit none

    call algorithms_initialize()

    call run_gemm_example() 

    call algorithms_finalize()
end program gemm_example
