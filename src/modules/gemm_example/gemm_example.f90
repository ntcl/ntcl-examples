module gemm_example_module
    use, intrinsic :: iso_fortran_env, only : real64

    use :: algorithms_api, only : ntcl_gemm

    implicit none

    public :: run_gemm_example

    integer, parameter :: m = 2
    integer, parameter :: n = 2
    integer, parameter :: k = 2
    integer, parameter :: lda = 2
    integer, parameter :: ldb = 2
    integer, parameter :: ldc = 2
    real(real64), parameter :: alpha = 1.0
    real(real64), parameter :: beta = 0.0
contains
    subroutine run_gemm_example()
        real(real64), dimension(lda, k) :: A
        real(real64), dimension(ldb, n) :: B
        real(real64), dimension(ldc, n) :: C

        A = reshape([1, 2, 3, 4], shape(A))

        B = reshape([4, 1, 2, 3], shape(B))
        call ntcl_gemm('N', 'N', m, n, k, alpha, A, lda, B, ldb, beta, C, ldc) 

        write (*,*) 'A:'
        call print_matrix(A)
        write (*,*) 'B:'
        call print_matrix(B)
        write (*,*) 'C:'
        call print_matrix(C)
    end subroutine run_gemm_example

    subroutine print_matrix(matrix)
        real(real64), dimension(:, :), intent(in) :: matrix

        integer :: row_index

        do row_index = 1, size(matrix, 1)
            write (*,*) matrix(row_index, :)
        end do
    end subroutine print_matrix

end module gemm_example_module
