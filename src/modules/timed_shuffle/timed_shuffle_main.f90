program main
    use :: util_api, only : &
            get_application_configuration

    use :: algorithms_initializer, only : &
            algorithms_initialize, &
            algorithms_finalize

    use :: timed_app_entry_module, only : timed_app_entry
    use :: timed_shuffle_app_module, only : timed_shuffle_app

    implicit none

    type(timed_app_entry) :: app

    call algorithms_initialize(get_application_configuration())

    app = timed_app_entry(timed_shuffle_app())
    call app%run()
    call app%cleanup()

    call algorithms_finalize()
end program main
